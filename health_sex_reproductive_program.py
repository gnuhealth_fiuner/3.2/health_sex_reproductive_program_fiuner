# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date

__all__ = ['SaludSexual','LecheMaternaMadres','LecheMaternaMenores', 'ExtraPrograma']


class SaludSexual(ModelSQL,ModelView):
    'Programa de Salud Sexual y Reproductiva'

    __name__ = 'programas.salud_sexual'

    fecha = fields.Date('Fecha de Entrega', required=True)
    
    usuario = fields.Many2One('gnuhealth.patient','Usuario', required=True)

    dni = fields.Function(fields.Char('DNI'),'get_dni')

    def get_dni(self, usuario):
        if self.usuario:
           return self.usuario.name.ref
 
        
    edad = fields.Function(fields.Float('Edad', help="Edad a la fecha de la entrega"),'get_patient_age')

    def get_patient_age(self, usuario):
        
        fecha_ent = self.fecha

        if self.usuario.name.dob:
            nac = self.usuario.name.dob
            delta = relativedelta(fecha_ent, nac)

            edad = (delta.years) + (delta.months/12.0) + (delta.days/365.0)
        else:
            edad = 0.0
        
        return float(edad)


    contacto = fields.Char('Medio de Contacto', size=40)

    producto1 = fields.Many2One('product.product', 'Producto Mes 1', help='Producto')

    cantidad = fields.Integer('Cantidad')

    producto2 = fields.Many2One('product.product', 'Producto Mes 2', help='Producto')

    cantidad2 = fields.Integer('Cantidad')

    producto3 = fields.Many2One('product.product', 'Producto Mes 3', help='Producto')

    cantidad3 = fields.Integer('Cantidad')


class LecheMaternaMadres(ModelSQL,ModelView):
    'Programa de entrega de leche a Madres'

    __name__ = 'programas.leche_materna_madres'

    fecha_control = fields.Date('Fecha de Control', required=True)

    fecha_entrega  = fields.Date('Fecha de Entrega',required=True)
    
    usuario = fields.Many2One('gnuhealth.patient','Apellido y Nombre', required=True)

    dni = fields.Function(fields.Char('DNI'),'get_dni', searcher='search_dni')

    def get_dni(self, usuario):
        if self.usuario:
           return self.usuario.name.ref    

    @classmethod
    def search_dni(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('usuario.name.ref', clause[1], value))
        return res


    edad = fields.Function(fields.Char('Edad', help="Edad a la fecha de la entrega"),'get_patient_age')

    def get_patient_age(self, usuario):
        nac = self.usuario.name.dob
        res = relativedelta(self.fecha_entrega, nac)
        return str(float(res.years) + float(str(res.months/12.0)))

    gestaciones = fields.Integer('Gestaciones', required=True)    

    partos = fields.Integer('Partos', required=True)  

    consultas = fields.Integer('Num Consultas', required=True)  

    sem_embarazo = fields.Integer('Sem. de embarazo', required=True)  

    peso = fields.Float('Peso [kg]', required=True)  

    talla = fields.Float('Talla [cm]', required=True)
    
    imc = fields.Function(fields.Float('IMC'), 'get_imc')

    def get_imc(self, name):
        imc = 0
        if self.peso and self.talla:
            imc = float(self.peso) / pow((float(self.talla)/100),2)
        return float(imc)

    diag_nut = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('BP', 'Bajo Peso'),
        ('SP', 'Sobrepeso'),
        ('OB', 'Obesidad'),
        ], 'Diag. Nutricional',
        required=True, sort=False)

    emb = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('P', 'Problematico'),
        ], 'Embarazo',
        required=True, sort=False)

    entrega_1 = fields.Integer('Cantidad  [Kg]')

    observaciones = fields.Char('Observaciones')


class LecheMaternaMenores(ModelSQL,ModelView):
    'Programa de entrega de leche a Menores'

    __name__ = 'programas.leche_materna_menores'

    fecha_control = fields.Date('Fecha de Control', required=True)
    
    fecha_entrega  = fields.Date('Fecha de Entrega', required=True)    

    usuario = fields.Many2One('gnuhealth.patient','Apellido y Nombre', required=True)

    dni = fields.Function(fields.Char('DNI'),'get_dni', searcher='search_dni')

    def get_dni(self, usuario):
        if self.usuario:
           return self.usuario.name.ref

    @classmethod
    def search_dni(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('usuario.name.ref', clause[1], value))
        return res



    fn = fields.Function(fields.Date('Fecha Nac.'),'get_fn')

    def get_fn(self, usuario):
        if self.usuario.name.dob:
           return self.usuario.name.dob  


    edad = fields.Function(fields.Float('Edad', help="Edad a la fecha de la entrega"),'get_patient_age')

    def get_patient_age(self, usuario):
        
        fecha_ent = self.fecha_entrega

        if self.usuario.name.dob:
            nac = self.usuario.name.dob
            delta = relativedelta(fecha_ent, nac)

            edad = float(delta.years) + float(delta.months/12.0) + float(delta.days/365.0)
        else:
            edad = 0.0
        
        return float(edad)
   

    peso_nacimiento = fields.Float('Peso al Nacer [Kg]', required=True)

    peso = fields.Float('Peso [Kg]', required=True)

    talla = fields.Float('Talla [cm]', required=True)
    
    @staticmethod
    def default_talla():
      return 45.0

    imc = fields.Float('IMC')
    
    @fields.depends('peso','talla')
    def on_change_with_imc(self):
        imc = 0
        if self.peso and self.talla:
            imc = float(self.peso) / pow((float(self.talla)/100),2)
        return float(imc)

    diag_nut = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('RBP', 'Riesgo Bajo Peso'),
        ('BP', 'Bajo Peso'),
        ('SP', 'Sobrepeso'),
        ('OB', 'Obesidad'),
        ('TB', 'Talla Baja'),
        ('CL', 'Crecimiento Lento'),
        ('CA', 'Crecimiento Acelerado'),
        ], 'Diag. Nutricional',
        required=True, sort=False)

    extra = fields.Integer('Extra Progr.')

    entrega_1 = fields.Integer('Cantidad  [Kg]')

    observaciones = fields.Char('Observaciones') 
  

class ExtraPrograma(ModelSQL,ModelView):
    'Entrega de Leche Extra Programa'

    __name__ = 'programas.extra_leche'

    fecha_entrega = fields.Date('Fecha Entrega', required=True)

    destino = fields.Char('Destinatario')

    cantidad = fields.Integer('Cantidad')





